# Readme

Test repo for UV4L load issue.

## Prerequisites

- Raspberry PI 3x
- Raspberry PI camera

### Flash Raspbian Buster Lite on an SD

Download here:

https://www.raspberrypi.org/downloads/raspbian/

Alternativley you might use `Raspbian Stretch`.

Before first boot and after having flashed the SD card with the image (e.g. using `Etcher`):

- Enable headless SSH by placing an empty `ssh` file into the boot partition `/boot` of the SD
- Enable headless Wifi by placing a `wpa_supplicant.conf` into the boot partition `/boot` of the SD.

```ini
country=<your-two-letter-code>
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
       ssid="<your-ssid>"
       psk="<your-password>"
       key_mgmt=WPA-PSK
}
```

After having plugged the SD to the PI and power cycled it, find the IP of the PI. Replace `192.168.189.0` with the base address of your network:

```bash
sudo nmap -sP 192.168.189.0/24 | awk '/^Nmap/{ip=$NF}/B8:27:EB/{print ip}'
```

After having found out the IP of the PI on your network SSH to the PI:

```bash
ssh pi@<ip-of-pi>
```

Initial password is `raspberry`.

### Configure the PI once you are at console level

```bash
sudo raspi-config
```

- Change user password. This is from then on your SSH password. **Strongly recommended**, if you let SSH open
- Interfacing options/Enable camera
- Advanced options: Expand file system
- Optional: Network options/Hostname: Change host name

### Update and Upgrade

```bash
sudo apt-get update
sudo apt-get upgrade -y
```

### Install UV4L

Install `UV4L` and the `WebRTC plugin for UV4L`. The U4VL service MUST be the only user of the Raspicam module:

```bash
curl http://www.linux-projects.org/listing/uv4l_repo/lpkey.asc | sudo apt-key add -
echo "deb http://www.linux-projects.org/listing/uv4l_repo/raspbian/stretch stretch main" | sudo tee -a /etc/apt/sources.list
sudo apt-get update
```

```bash
sudo apt-get install -y uv4l uv4l-raspicam uv4l-raspicam-extras uv4l-webrtc uv4l-raspidisp uv4l-raspidisp-extras
```

Edit UV4L config:

```bash
sudo nano /etc/uv4l/uv4l-raspicam.conf
```

Add this at the end of the file: (leave options with the same name unchanged in the file and commented as is)

```ini
server-option = --port=8080
server-option = --use-ssl=no  
server-option = --enable-webrtc-datachannels=no
server-option = --enable-webrtc-audio=no
server-option = --webrtc-receive-video=no
server-option = --webrtc-receive-datachannels=no
server-option = --webrtc-receive-audio=no
server-option = --webrtc-tcp-candidate-policy=0
server-option = --webrtc-hw-vcodec-sps-pps-idr=yes
```

Enable syslog:

```ini
verbosity = 6
syslog-host = localhost
syslog-port = 514
```

Reboot UV4L to take over configuration. UV4L is running as a service, so it will be there again after PI's reboot.

```bash
sudo service uv4l_raspicam restart
```

### Install NodeJS

Install NodeJS. Choose something >= Node 8

```bash
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt-get install -y nodejs
```

### Install the test app

```bash
cd ~
sudo apt-get install -y git
git clone https://bitbucket.org/neilyoung2512/test-uv4l.git
cd test-uv4l
npm install
```

### Run the test

```bash
node server.js
```

From another SSH console:

To start:

```bash
curl localhost:3000/start
```

To ping:

```bash
curl localhost:3000/ping
```

To stop:

```bash
curl localhost:3000/stop
```

From another SSH console:

```bash
htop
```

Watch memory consumption of UV4L growing.

From another console:

```bash
tail -f /var/log/syslog
```

Watch syslog. Access is granted.
