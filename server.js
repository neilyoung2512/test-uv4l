let WebSocket = require('ws')

function forceGC() {
    if (global.gc) {
        global.gc()
    } else {
        console.warn('No GC hook! Start as `node --expose-gc server.js`')
    }
}

async function test() {
    let socket = new WebSocket(`http://localhost:8080/stream/webrtc`)

    socket.onmessage = (evt) => {
        let data = JSON.parse(evt.data)
        if (data.what == 'offer') {
            console.log("offered")
            socket.send(JSON.stringify({
                what: 'hangup',
               
            }), (err) => {
                console.log("hungup")
                socket.close()
                if (err) {
                    console.log(err)
                    process.exit()
                }
            })

        }
    }

    socket.onclose = () => {
        console.log("closed")
        process.exit()
    }

    socket.onerror = (error) => {
        console.log(`error ${JSON.stringify(error)}`)
    }

    socket.onopen = () => {
        console.log("opened")
        socket.send(JSON.stringify({
            what: 'call',
            options: {
                force_hw_vcodec: true,
                vformat: 30,
                trickle_ice: true
            }
        }), (err) => {
                console.log("called")
                if (err) {
                    console.log(err)
                    process.exit()
                }     
            })
    }
}

(async () => {
    await test()
})()

