i=1
echo "New test series" >> "result.txt"
while(true); do
  echo ""
  now=`date`
  echo "---- Round "$i" -----"
  node server.js $i
  sudo service uv4l_raspicam restart
  sleep 0.3
  ((i=i+1))
done
